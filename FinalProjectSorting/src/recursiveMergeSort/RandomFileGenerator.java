/*
 * File:	RandomFileGenerator.java
 * Created:	Sept. 24, 2020
 * 
 * Author: 	Christian Blosser
 * 
 * Description:
 * 		RandomFileGenerator java class simply creates text files with random numbers.
 * 		This will receive action from the Runner class to create the files of given
 * 		sizes.  These files will then be buffered into the program and sorted.
 */
package recursiveMergeSort;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class RandomFileGenerator
{
	public RandomFileGenerator()
	{

	}
	
	//printRandom(int, int) creates files of defined size and prints random numbers to them
	//@param numFiles the number of files to be created
	//@param sizeFiles the amount of numbers to be printed to a file
	public void printRandom(int numFiles, int sizeFiles) throws FileNotFoundException
	{
		for (int i = 1; i < numFiles+1; i++)
		{
			PrintWriter pw = new PrintWriter("Random_" + sizeFiles + "_" + i + ".txt");
			for (int j = 0; j < sizeFiles; j++)
			{
				int randomNumber = (int) (Math.floor(Math.random() * 10000));
				pw.print(randomNumber + " ");
			}
			pw.close();
		}
	}
}
