/*
 * File:	MergeSort.java
 * Created:	Sept. 23, 2020
 * 
 * Author: 	Christian Blosser
 * 
 * Description:
 * 		MergeSort java class implements the merge sort algorithm recursively.  Merge sort algorithm is made up of
 *		two methods: mergesort(int[]) and merge(int[], int[], int[]).  The merge sort alogrithm functions has an
 *		efficiency rating of O(n log n) at worst case.
 */

package recursiveMergeSort;

public class MergeSort
{

	public MergeSort()
	{
		
	}

	/**
	 * mergesort(int[]) takes one parameter of an int array.  Array sort executes by checking if the parameter array
	 * is long enough to sort.  If this length falls under 1, the array can no longer be split into smaller arrays.
	 * During each iteration
	 * @param arrayToSort intake of the array wished to be sorted
	 */
	public void mergesort(int[] arrayToSort)
	{
		if (arrayToSort.length > 1)
		{
			int arrayToSortSize = arrayToSort.length;
			//added a midway variable to determine the halfway point
			int middleOfArrayToSort = arrayToSortSize/2;

			//declare first array space
			int[] firstHalfArray = new int[middleOfArrayToSort];

			//declare second array space
			//for odd array sizes, the second half needs to be a difference
			//I.e. array size of 9, half is 4, size - half is 5.
			int[] secondHalfArray = new int[arrayToSortSize - middleOfArrayToSort];

			//divide the initial array into equal parts
			for (int i = 0; i < middleOfArrayToSort; i++)
			{
				firstHalfArray[i] = arrayToSort[i];
			}
			for (int i = middleOfArrayToSort; i < arrayToSort.length; i++)
			{
				secondHalfArray[i - middleOfArrayToSort] = arrayToSort[i];
			}

			//mergesort recursively the first half
			mergesort(firstHalfArray);
			//mergesort recursively the second half
			mergesort(secondHalfArray);
			//combine/merge the first half and seocond half
			merge(firstHalfArray, secondHalfArray, arrayToSort);
		}
	}

	//merge(int[], int[], int[]) is the critcial method of merge sort.  The task of merge is to  merge the two
	//sub-arrays (firstHalfArray, secondHalfArray). 
	//@param firstHalfArray passed through from recursive mergesort
	//@param secondHalfArray passed through from recursive mergesort
	//@param arrayToSort passed through from recurisve mergesort
	public void merge(int[] firstHalfArray, int[] secondHalfArray, int[] arrayToSort)
	{
		//variales to progress through the loops used to merge the separated arrays
		int firstHalfArrayCounter, secondHalfArrayCounter, arrayToSortCounter;
		firstHalfArrayCounter = 0; secondHalfArrayCounter = 0; arrayToSortCounter = 0;

		//organize the array in order, lowest number gets stored
		while (firstHalfArrayCounter < firstHalfArray.length && secondHalfArrayCounter < secondHalfArray.length)
		{
			//compare the two arrays, lowest number gets stored
			if (firstHalfArray[firstHalfArrayCounter] <= secondHalfArray[secondHalfArrayCounter])
			{
				arrayToSort[arrayToSortCounter] = firstHalfArray[firstHalfArrayCounter]; firstHalfArrayCounter++;
			}else
			{
				arrayToSort[arrayToSortCounter] = secondHalfArray[secondHalfArrayCounter]; secondHalfArrayCounter++;
			}
			//increment the counter
			arrayToSortCounter++;
		}

		//check if the end of the array has been reached
		if (firstHalfArrayCounter == firstHalfArray.length)
		{
			//finish adding remaining elements from second half
			for (int temp = secondHalfArrayCounter; temp < secondHalfArray.length && arrayToSortCounter < arrayToSort.length; temp++)
			{
				arrayToSort[arrayToSortCounter] = secondHalfArray[temp]; arrayToSortCounter++;
			}
		}else	//otherwise
		{
			//finish adding remaining elemets from first half
			for (int temp = firstHalfArrayCounter; temp < firstHalfArray.length && arrayToSortCounter < arrayToSort.length; temp++)
			{
				arrayToSort[arrayToSortCounter] = firstHalfArray[temp]; arrayToSortCounter++;
			}
		}
	}
}
