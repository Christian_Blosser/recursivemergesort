/*
 * File:	Runner.java
 * Created:	Sept. 24, 2020
 * 
 * Author: 	Christian Blosser
 * 
 * Description:
 * 		The runner class is responsible for calling the other classes and printing the sorted files.
 * 		When the files have all been sorted and printed, it prints a time results file displaying
 * 		all of the mergesort run times.
 */
package recursiveMergeSort;
import java.util.Scanner;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Runner
{
	//intialize necessary arrays for buffers and run times
	private static long[] timeResultsArray = new long[270];
	private static int[] randomNumberArray;
	//intialze final values for amount of numbers in the files
	private static final int SMALL = 10000;
	private static final int MEDIUM = 100000;
	private static final int LARGE = 1000000;

	//main(String[]) is responsible for creating instances of the other classes, calling the other
	//classes, and throwing an exception if need be.  The process is print to file, read from file
	//and store into buffer arrays, mergesort, record time, write to files, and print times.
	//@param args input parameters from console
	public static void main(String[] args) throws FileNotFoundException
	{
		//create objects of mergeSort and randomFileGenerator
		MergeSort ms = new MergeSort();
		RandomFileGenerator rfg = new RandomFileGenerator();
		
		//print 30 files of 10,000 numbers
		rfg.printRandom(30, SMALL);
		
		//loop through each file following process.
		for (int i = 1; i < 31; i++)
		{
			//declare buffer of SMALL size
			randomNumberArray = new int[SMALL];
			int count = 0;
			//read previously created file
			Scanner sc = new Scanner(new FileInputStream("Random_" + SMALL + "_" + i + ".txt"));
			//while something still exists in the file
			while (sc.hasNextInt())
			{
				//read into buffer int by int and increment counter
				randomNumberArray[count] = sc.nextInt();
				count++;
			}
			//close scanner
			sc.close();
			//begin 'stop-watch'
			long startTime = System.currentTimeMillis();
			//call and execute mergesort on array
			ms.mergesort(randomNumberArray);
			//terminate 'stop-watch' and add to time results array
			long endTime = System.currentTimeMillis();
			timeResultsArray[i] = endTime - startTime;
			
			//print the results of sorting to a respectively named file
			for (int j = 0; j < 1; j++)
			{
				PrintWriter pw = new PrintWriter("RandomResult_" + SMALL + "_" + i + ".txt");
				for (int k = 0; k < SMALL; k++)
				{
					pw.print(randomNumberArray[k] + " ");
				}
				pw.close();
			}
		}
	
		//	PROCESS IS REPEATED FOR MEDIUM AND LARGE FILE SIZES
		
		rfg.printRandom(30, MEDIUM);
		for (int i = 1; i < 31; i++)
		{
			randomNumberArray = new int[MEDIUM];
			int count = 0;
			Scanner sc = new Scanner(new FileInputStream("Random_" + MEDIUM + "_" + i + ".txt"));
			while (sc.hasNextInt())
			{
				randomNumberArray[count] = sc.nextInt();
				count++;
			}
			sc.close();
			long startTime = System.currentTimeMillis();
			ms.mergesort(randomNumberArray);
			long endTime = System.currentTimeMillis();
			timeResultsArray[i+30] = endTime - startTime;
			
			for (int j = 0; j < 1; j++)
			{
				PrintWriter pw = new PrintWriter("RandomResult_" + MEDIUM + "_" + i + ".txt");
				for (int k = 0; k < MEDIUM; k++)
				{
					pw.print(randomNumberArray[k] + " ");
				}
				pw.close();
			}
		}
		
		rfg.printRandom(30, LARGE);
		for (int i = 1; i < 31; i++)
		{
			randomNumberArray = new int[LARGE];
			int count = 0;
			Scanner sc = new Scanner(new FileInputStream("Random_" + LARGE + "_" + i + ".txt"));
			while (sc.hasNextInt())
			{
				randomNumberArray[count] = sc.nextInt();
				count++;
			}
			sc.close();
			long startTime = System.currentTimeMillis();
			ms.mergesort(randomNumberArray);
			long endTime = System.currentTimeMillis();
			timeResultsArray[i+60] = endTime - startTime;
			
			for (int j = 0; j < 1; j++)
			{
				PrintWriter pw = new PrintWriter("RandomResult_" + LARGE + "_" + i + ".txt");
				for (int k = 0; k < LARGE; k++)
				{
					pw.print(randomNumberArray[k] + " ");
				}
				pw.close();
			}
		}
		
		//section to print time results to a respectively named txt file
		//create file with print writer
		PrintWriter p = new PrintWriter("Time_Results.txt");
		
		//separate the file sizes by the times
		p.println("Time Results for 30 random SMALL files: Mergesort");
		//loop through time results array and print each time in milliseconds
		for (int i = 0; i < 90; i++)
		{
			if (i == 30)
			{
				p.println("TimeResults for 30 random MEDIUM files: Mergesort");
			}
			if (i == 60)
			{
				p.println("TimeResults for 30 random LARGE files: Mergesort");
			}
			p.println("Trial_" + (i+1) + ":   " + timeResultsArray[i] + "ms");
		}
		p.close();
	}
}
